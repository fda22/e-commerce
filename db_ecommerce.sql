-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2021 at 09:09 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_04_18_101953_create_products_table', 1),
(5, '2020_04_18_132841_create_profiles_table', 1),
(6, '2020_04_21_154729_create_stocks_table', 1),
(7, '2020_04_24_084350_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `user_id`, `name`, `cart`, `phonenumber`, `city`, `address`, `zipcode`, `payment_id`) VALUES
(3, '2021-02-06 22:27:23', '2021-02-06 22:27:23', 5, 'Rini Inggriani', 'O:8:\"App\\Cart\":3:{s:5:\"items\";a:1:{i:0;a:5:{s:8:\"quantity\";i:1;s:5:\"price\";i:5000;s:4:\"size\";s:2:\"34\";s:4:\"item\";O:11:\"App\\Product\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:8:\"products\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:10:{s:2:\"id\";i:20;s:4:\"name\";s:4:\"test\";s:5:\"brand\";s:4:\"Nike\";s:5:\"price\";i:5000;s:5:\"image\";s:53:\"products/xApRLT3Cssa7icTBdYoSXZNHfzkS2mIFDCOzsWiN.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-02-07 05:26:17\";s:10:\"updated_at\";s:19:\"2021-02-07 05:26:17\";}s:11:\"\0*\0original\";a:10:{s:2:\"id\";i:20;s:4:\"name\";s:4:\"test\";s:5:\"brand\";s:4:\"Nike\";s:5:\"price\";i:5000;s:5:\"image\";s:53:\"products/xApRLT3Cssa7icTBdYoSXZNHfzkS2mIFDCOzsWiN.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-02-07 05:26:17\";s:10:\"updated_at\";s:19:\"2021-02-07 05:26:17\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}s:10:\"product_id\";i:20;}}s:13:\"totalQuantity\";i:1;s:10:\"totalPrice\";i:5000;}', '0888888888888', 'bandung', 'Komp. Bukit Pajajaran 412', 409271, NULL),
(4, '2021-02-07 00:18:57', '2021-02-07 00:18:57', 5, 'farhan', 'O:8:\"App\\Cart\":3:{s:5:\"items\";a:1:{i:0;a:5:{s:8:\"quantity\";i:1;s:5:\"price\";i:5000;s:4:\"size\";s:2:\"34\";s:4:\"item\";O:11:\"App\\Product\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:8:\"products\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:10:{s:2:\"id\";i:20;s:4:\"name\";s:4:\"test\";s:5:\"brand\";s:4:\"Nike\";s:5:\"price\";i:5000;s:5:\"image\";s:53:\"products/xApRLT3Cssa7icTBdYoSXZNHfzkS2mIFDCOzsWiN.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-02-07 05:26:17\";s:10:\"updated_at\";s:19:\"2021-02-07 05:26:17\";}s:11:\"\0*\0original\";a:10:{s:2:\"id\";i:20;s:4:\"name\";s:4:\"test\";s:5:\"brand\";s:4:\"Nike\";s:5:\"price\";i:5000;s:5:\"image\";s:53:\"products/xApRLT3Cssa7icTBdYoSXZNHfzkS2mIFDCOzsWiN.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-02-07 05:26:17\";s:10:\"updated_at\";s:19:\"2021-02-07 05:26:17\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}s:10:\"product_id\";i:20;}}s:13:\"totalQuantity\";i:1;s:10:\"totalPrice\";i:5000;}', '0888809582739', 'bandung', 'test', 40971, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('farhan.anggara2212@gmail.com', '$2y$10$Zv0EsGaG55zE4kZcu0GRgeuWY4Oc0xVBl.ux7vGUX2i3x9GUj4e2O', '2021-02-04 02:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `brand`, `price`, `image`, `gender`, `category`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 'AIR JORDAN 1 X OFF-WHITE NRG \"OFF WHITE UNC\"', 'Nike', 1375, 'products/1.jpg', 'Female', 'Shoes', 1, NULL, NULL),
(2, 'STUSSY X AIR ZOOM SPIRIDON CAGED \"PURE PLATINUM\"', 'Nike', 225, 'products/2.jpg', 'Unisex', 'Shoes', 12, NULL, NULL),
(3, 'SUPREME X AIR FORCE 1 LOW \"BOX LOGO - WHITE\"', 'Nike', 275, 'products/3.jpg', 'Male', 'Shoes', 1, NULL, NULL),
(4, 'SACAI X LDV WAFFLE \"BLACK NYLON\"', 'Nike', 190, 'products/4.jpg', 'Male', 'Shoes', 1, NULL, NULL),
(5, 'AIR JORDAN 1 RETRO HIGH \"SHATTERED BACKBOARD\"', 'Nike', 980, 'products/5.jpg', 'Male', 'Shoes', 14, NULL, NULL),
(6, 'YEEZY BOOST 350 V2 \"CREAM\"', 'Adidas', 780, 'products/6.jpg', 'Unisex', 'Shoes', 3, NULL, NULL),
(7, 'YEEZY BOOST 350 V2\"YECHEIL NON-REFLECT\"', 'Adidas', 978, 'products/7.jpg', 'Male', 'Shoes', 5, NULL, NULL),
(8, 'YEEZY BOOST 350 V2 \"FROZEN YELLOW\"', 'Adidas', 1100, 'products/8.jpg', 'Unisex', 'Shoes', 3, NULL, NULL),
(9, 'AIR JORDAN 5 RETRO SP \"MUSLIN\"', 'Nike', 1499, 'products/9.jpg', 'Male', 'Shoes', 3, NULL, NULL),
(10, 'AIR JORDAN 1 RETRO HIGH ZOOM \"RACER BLUE\"', 'Nike', 625, 'products/10.jpg', 'Male', 'Shoes', 5, NULL, NULL),
(11, 'FENTY SLIDE \"PINK BOW \"', 'Puma', 399, 'products/11.jpg', 'Female', 'Shoes', 3, NULL, NULL),
(12, 'WMNS RS-X TRACKS \"FAIR AQUA\"', 'Puma', 499, 'products/12.jpg', 'Female', 'Shoes', 3, NULL, NULL),
(13, 'OLD SKOOL \'BLACK WHITE\' \"BLACK WHITE\"', 'Vans', 239, 'products/13.jpg', 'Unisex', 'Shoes', 6, NULL, NULL),
(14, 'OLD SKOOL \"YACHT CLUB\"', 'Vans', 359, 'products/14.jpg', 'Unisex', 'Shoes', 5, NULL, NULL),
(15, 'VANS OLD SKOOL \"RED CHECKERBOARD \"', 'Vans', 419, 'products/15.jpg', 'Unisex', 'Shoes', 5, NULL, NULL),
(16, 'ALL STAR 70S HI \"MILK\"', 'Converse', 579, 'products/16.jpg', 'Unisex', 'Shoes', 5, NULL, NULL),
(17, 'ALL-STAR 70S HI \"PLAY\"', 'Puma', 619, 'products/17.jpg', 'Unisex', 'Shoes', 3, NULL, NULL),
(18, 'FEAR OF GOD CHUCK 70 HI \"NATURAL\"', 'Converse', 1259, 'products/18.jpg', 'Female', 'Shoes', 5, NULL, NULL),
(20, 'test', 'Nike', 5000, 'products/xApRLT3Cssa7icTBdYoSXZNHfzkS2mIFDCOzsWiN.jpg', 'Male', 'Shoes', 1, '2021-02-06 22:26:17', '2021-02-06 22:26:17'),
(21, 'test', 'Nike', 20000000, 'products/JYDlrUG2oTYMEaWhcUcykJ0dWBCJjfDRSeaVUzQs.jpg', 'Female', 'Shoes', 1, '2021-02-07 00:23:53', '2021-02-07 00:23:53'),
(22, 'farhan', 'Fila', 9000000, 'products/umSdQTVS3nK5oXRb6jokZMIqZ48ITbYCpKjBw4jW.png', 'Male', 'Shoes', 1, '2021-02-07 00:24:24', '2021-02-07 00:24:24');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phonenumber` bigint(20) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reminder` text COLLATE utf8mb4_unicode_ci DEFAULT 'Type Something',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reminders`
--

INSERT INTO `reminders` (`id`, `reminder`, `created_at`, `updated_at`) VALUES
(1, 'Type something', '2021-02-06 03:16:17', '2021-02-06 03:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `name`, `quantity`, `created_at`, `updated_at`) VALUES
(59, 20, '44', 9, '2021-02-07 00:24:42', '2021-02-07 00:24:42'),
(60, 20, '44', 9, '2021-02-07 00:24:51', '2021-02-07 00:24:51'),
(61, 21, '44', 9, '2021-02-07 00:25:03', '2021-02-07 00:25:03'),
(62, 22, '44', 9, '2021-02-07 00:25:17', '2021-02-07 00:25:17'),
(63, 1, '32', 0, NULL, NULL),
(64, 1, '33', 10, NULL, NULL),
(65, 1, '34', 10, NULL, NULL),
(66, 1, '35', 10, NULL, NULL),
(67, 1, '36', 10, NULL, NULL),
(68, 1, '37', 10, NULL, NULL),
(69, 1, '38', 10, NULL, NULL),
(70, 2, '39', 10, NULL, NULL),
(71, 2, '40', 10, NULL, NULL),
(72, 2, '32', 0, NULL, NULL),
(73, 2, '33', 10, NULL, NULL),
(74, 2, '34', 10, NULL, NULL),
(75, 2, '35', 10, NULL, NULL),
(76, 3, '36', 10, NULL, NULL),
(77, 3, '37', 10, NULL, NULL),
(78, 3, '38', 10, NULL, NULL),
(79, 3, '39', 10, NULL, NULL),
(80, 3, '40', 10, NULL, NULL),
(81, 4, '32', 0, NULL, NULL),
(82, 4, '33', 10, NULL, NULL),
(83, 4, '34', 10, NULL, NULL),
(84, 4, '35', 10, NULL, NULL),
(85, 4, '36', 10, NULL, NULL),
(86, 4, '37', 10, NULL, NULL),
(87, 4, '38', 10, NULL, NULL),
(88, 5, '39', 10, NULL, NULL),
(89, 5, '40', 10, NULL, NULL),
(90, 5, '32', 0, NULL, NULL),
(91, 5, '33', 10, NULL, NULL),
(92, 6, '34', 10, NULL, NULL),
(93, 6, '35', 10, NULL, NULL),
(94, 6, '36', 10, NULL, NULL),
(95, 6, '37', 10, NULL, NULL),
(96, 7, '38', 10, NULL, NULL),
(97, 7, '39', 10, NULL, NULL),
(98, 7, '40', 10, NULL, NULL),
(99, 7, '32', 0, NULL, NULL),
(100, 7, '33', 10, NULL, NULL),
(101, 7, '34', 10, NULL, NULL),
(102, 8, '35', 10, NULL, NULL),
(103, 8, '36', 10, NULL, NULL),
(104, 8, '37', 10, NULL, NULL),
(105, 8, '38', 10, NULL, NULL),
(106, 9, '39', 10, NULL, NULL),
(107, 9, '40', 10, NULL, NULL),
(108, 9, '32', 0, NULL, NULL),
(109, 10, '33', 10, NULL, NULL),
(110, 11, '34', 10, NULL, NULL),
(111, 12, '35', 10, NULL, NULL),
(112, 13, '36', 10, NULL, NULL),
(113, 14, '37', 10, NULL, NULL),
(114, 15, '38', 10, NULL, NULL),
(115, 16, '39', 10, NULL, NULL),
(116, 17, '39', 10, NULL, NULL),
(117, 17, '40', 10, NULL, NULL),
(118, 18, '38', 10, NULL, NULL),
(119, 18, '39', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Admin','Customer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$yH23bm7Nh7ievBJ0M.5CKOdJh9arXLDXCsxj7vNsc8qg3pLc77JkW', 'Admin', 'Lv2H8yCjLjxp6mUvDbcy9BhdSWf37ZvnkQ7FufJseZSJcFlico1v5B2d5ruv', NULL, NULL),
(5, 'farhan', 'farhan.anggara2212@gmail.com', NULL, '$2y$10$yH23bm7Nh7ievBJ0M.5CKOdJh9arXLDXCsxj7vNsc8qg3pLc77JkW', 'Customer', NULL, '2021-02-04 02:01:37', '2021-02-04 02:01:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_index` (`user_id`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_product_id_index` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
