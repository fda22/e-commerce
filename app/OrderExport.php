<?php
  
namespace App;
  
use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
  
class OrderExport implements FromCollection
{
    public function collection()
    {
        return Order::all();
    }
}