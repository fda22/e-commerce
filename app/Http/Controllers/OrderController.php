<?php

namespace App\Http\Controllers;
use App\Order;
use App\OrderExport;
use Auth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
    public function show(){
        $orders = Auth::user()->orders;
        $orders->transform(function($order,$key){
            $order->cart = unserialize($order->cart);
            return $order;
        });
        return view('orders.index',compact(['orders']));
    }
    public function export() 
    {
        return Excel::download(new OrderExport, 'purchase.xlsx');
    }
}